<?
    //$db_param=('localhost', 'syacencc_test', '123456', 'syacencc_test');
    class db{
        public static $db;
        public static function query($query){        
            return self::$db->query($query, $resultmode);
        }

        //singleton
        protected static $_instance;

        private function __construct() {
            self::$db=new mysqli('localhost', 'syacencc_test2', '123456', 'syacencc_test2');
        }
        
        function __destruct(){
            self::$db->close();
        }

        public static function getInstance() {
            if (self::$_instance === null) {
                self::$_instance = new self;   
            }
     
            return self::$_instance;
        }
      
        private function __clone() {
        }

        private function __wakeup() {
        }
        //////////////
   }
   db::getInstance();
?>
