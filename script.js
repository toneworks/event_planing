$(document).ready(function(){
    var uniqid=document.cookie.match(/uniqid=(\w+)/)||{}[1]
    if(uniqid)get_authorized_page(uniqid);
})

function listen_status(uniqid){
    $.ajax({
        type: "GET",
        url: "listen_status.php",
        success: function(answer){
            get_authorized_page(uniqid)
        },
        error: function(error){
            setTimeout(listen_status, 1000, uniqid)
        }
    })
}

function get_authorized_page(uniqid){
    $('[name=\'entering\']').fadeIn();
    $.ajax({
        type: "GET",
        url: "authorized_page.php",
        data: {uniqid: uniqid},
        success: function(answer){
            $(".main").html(answer)
            listen_status(uniqid)
        },
        error: function(error){
            alert('Ошибка сервера (authorized_page):'+error.status)
            listen_status(uniqid)
        }
    })
}

function send_form(){
    var data=$(".enter").serialize()
    $.ajax({
        type: "GET",
        url: "enter.php",
        data: data,
        success: function(answer){
            if(answer.match(/wrong_password/g)){
                alert("Неверный пароль от зарегестрированного пользователя")
                return
            }
            if(answer.match(/new_user/g))alert("Пользователь \'"+$("[name=\'name\']").val()+" успешно зарегестрирован");
            get_authorized_page(answer.match(/uniqid:(\w+)/)[1])
        },
        error: function(error){alert('Ошибка сервера:'+error.status)}
    })
}

function start_voiting(){
    var data=$(".opening_voiting").serialize()
    $('[name=\'requesting\']').fadeIn()
    $.ajax({
        type: "POST",
        url: "starting_voiting.php",
        data: data,
        success: function(answer){
            if(answer=="voiting already open")alert("Голосование уже начато другим пользователем!")
            $('[name=\'requesting\']').fadeOut()
        },
        //error: function(error, exception){alert(error+exception)}
    })

}

function reset_voiting(){
    $.ajax({
        type: "POST",
        url: "reset_voiting.php",
        success: function(answer){alert("Голосование прекращено")}
    })
}

function setCookie(name, value, options) {
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for (var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];
    if (propValue !== true) {
      updatedCookie += "=" + propValue;
    }
  }

  document.cookie = updatedCookie;
}

function deleteCookie(name) {
  setCookie(name, "", {
    expires: -1
  })
}

function exit_user(){
    deleteCookie("uniqid")
    location.reload()
}
