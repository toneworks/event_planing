<?
require_once $_SERVER['DOCUMENT_ROOT'].'/db.php';
$votes=db::query('select event_time, event from votes order by event_time')->fetch_all();
$result_time=array();
$result_event=array();

//Высчитываем таблицу для диаграммы для внутренней части - времени
//По принципу - чем больше данное время раз попалось тем больше его вес на диаграмме с соответствующими событиями
foreach($votes as $vote){
    $already_exist=false;
    foreach($result_time as $key => $time)
        if(strcmp($time[0], $vote[0])==0){
            $result_time[$key][1]++;
            $already_exist=true;
            break;
        }
    if(!$already_exist)array_push($result_time, array($vote[0], 1));
}
//Высчитываем таблицу для событий для внешней части
foreach($result_time as $time){
    $t=$time[0];
    $events_in_time=db::query("select event from votes where event_time='$t' order by event_time" )->fetch_all();
    $res=array();
    foreach($events_in_time as $event){
        $already_exist=false;
        foreach($res as $key => $res_event){
            if(strcmp($res_event[0], $event[0])==0){
                $res[$key][1]++;
                $already_exist=true;
                break;
            }
        }
        if(!$already_exist)array_push($res, array($event[0], 1));
    }

    foreach($res as $value)
        array_push($result_event, $value);
}

$result=array($result_time, $result_event);
echo json_encode($result);
?>
