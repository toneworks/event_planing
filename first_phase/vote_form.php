<form name="voiting" action="javascript:vote()">
<label for="time" class="field">Время мероприятия</label>
<select name="time" class="field">
<?
for($h=getdate()['hours']; $h<=23; $h++){
    if($h==getdate()['hours'])$mm=floor(getdate()['minutes']/15)+1;
    else $mm=0;
    $mm_max=($h<23?3:0);
    while($mm<=$mm_max){
        $m=$mm*15;
        $m=($m>0?$m:'00');
        echo '<option>'.$h.':'.$m.'</option>';
        $mm++;
    }
}
?>
</select>
<div>
    <label for="event" class="field" style="text-align: left">Мероприятие:</label>
    <input required name="event" class="field" type="text" style="margin: 0" autocomplete="Off">
<select name="event_options" class="event_options" size=7>
    <?require 'event_options.php';
    ?>
</select>
</div>
<input type="submit" value="Голосовать" class="field">
</form>
<script>
    function vote(){
        $.ajax({
            type: "GET",
            url: "first_phase/voiting.php",
            data: {
                event: $('[name=\'event\']').val(),
                time: $('[name=\'time\']').val(),
            },
            success: function(answer){
                $('[name=\'voiting\']').html(answer)
            },
            error: function(error){
                alert("error voiting")
            }
        })
    }
    
    $('[name=\'event_options\']').on('change', function(){
        $('[name=\'event\']').val($('[name=\'event_options\']').val())
    })
    
    $('[name=\'event\']').keyup(function(eventObject){
        $.ajax({
            type: "GET",
            url: "first_phase/event_options.php",
            data: {input_value: $('[name=\'event\']').val()},
            success: function(answer){
                $('[name=\'event_options\']').html(answer)
            },
//            error: function(error){alert("error:"+error.status)}
        })
    })
    $('[name=\'event\']').keydown(function(eventObject){
        if(eventObject.which==40){
            $('[name=\'event_options\']').focus();
            
        }
    })
</script>
