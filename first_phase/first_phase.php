<h1>Голосование: первая фаза</h1>
<div class="timer" name="timer"><?require $_SERVER['DOCUMENT_ROOT'].'/gettimer.php'?></div>
<div class="timer" style="margin-left: 20px;"><span name="vote_count" style="margin: 5px"></span><image src="pics/person.png" style="position: relative; top: 5; width: 20; height=20;"></div><p></p>

<?require_once $_SERVER['DOCUMENT_ROOT'].'/db.php';
$vote_data=db::query("select event, event_time from votes where user=".$username)->fetch_all();
if(count($vote_data)>0)require_once 'already_voited.php';
else require 'vote_form.php'?>
<script>
    function listen_new_events(){
        $.ajax({
            type: "GET",
            url: "first_phase/listen_new_events.php",
            success: function(answer){
                $('[name=\'event_options\']').html(answer)
                listen_new_events()
            },
            error: function(){
                setTimeout(listen_new_events, 2000)
            }
        })
    }
    listen_new_events()
    
    function listen_vote_count(){
         $.ajax({
            type: "GET",
            url: "first_phase/listen_vote_count.php",
            data: {count: $('[name=\'vote_count\']').val()},
            success: function(answer){
                $('[name=\'vote_count\']').html(answer)
                listen_vote_count()
            },
            error: function(error){
                setTimeout(listen_vote_count, 2000)
            }
        })
    }
    listen_vote_count()
    
    function timer(){
        $.ajax({
            type: "GET",
            url: "gettimer.php",
            success: function(answer){
                $('[name=\'timer\']').html(answer)
                timer()
            },
            error: function(error){
                error("timer error")
                setTimeout(timer, 2000)
            }
        })
    }
    timer()
    

</script>
